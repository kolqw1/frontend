var menuObject = undefined;

function ulAppend(ancestor, texts)
{
    ancestor.classList.add("selected");
    const ul = document.createElement("ul");
    for (/** @type string*/text of texts)
    {
        const li = document.createElement("li");
        const a = document.createElement("a");
        a.appendChild(document.createTextNode(text));
        a.setAttribute("href", "/");
        li.appendChild(a);
        ul.appendChild(li);
    }
    ancestor.appendChild(ul);
}

function hide(node)
{
    node.classList.remove("selected");
    node.lastChild.style.display = "none";
}

function show(node)
{
    node.classList.add("selected");
    node.lastChild.style.display = "block";
}

function addClickListener(id, idLi)
{
    var selected = false;
    var inDom = false;//false if ul hasn't been appended yet
    const liElement = document.getElementById(idLi);
    document.getElementById(id).addEventListener("click", event => {
        if (!menuObject)
        {
            fetch("/menu.json").then(function(response){
                response.json().then(resp => {
                    menuObject = resp;
                    ulAppend(liElement, menuObject[id]);
                    inDom = true;
                });
            });
        } else {
            if (!selected) {
                if (inDom) {
                    show(liElement);
                } else {
                    ulAppend(liElement, menuObject[id]);
                    inDom = true;
                }
            } else {
                hide(liElement);
            }
        }
        selected = !selected;
        event.preventDefault();
    });
}

document.addEventListener("DOMContentLoaded", () => {
    addClickListener("View",    "ViewLi");
    addClickListener("Graph",   "GraphLi");
    addClickListener("Colors",  "ColorsLi");
    addClickListener("Export",  "ExportLi");
    addClickListener("Math",    "MathLi");
    addClickListener("Actions", "ActionsLi");
});
